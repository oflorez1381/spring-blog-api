package com.blog.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.blog.api.domain.Post;

public interface PostRepository extends PagingAndSortingRepository<Post,Long>{

}
