package com.blog.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.blog.api.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {

	public User findByUsername(String username);

}
