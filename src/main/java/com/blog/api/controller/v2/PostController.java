package com.blog.api.controller.v2;

import java.net.URI;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.blog.api.domain.Post;
import com.blog.api.dto.error.ErrorDetail;
import com.blog.api.exception.ResourceNotFoundException;
import com.blog.api.repository.PostRepository;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@RestController("postControllerV2")
@RequestMapping("/v2/")
@Api(value = "posts", description = "Posts API")
public class PostController {

	@Inject
	private PostRepository postRepository;

	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	@ApiOperation(value = "Retrieves all the posts", response = Post.class, responseContainer = "List")
	public ResponseEntity<Iterable<Post>> getAllAction(Pageable pageable) {
		Page<Post> posts = postRepository.findAll(pageable);
		return new ResponseEntity<>(posts, HttpStatus.OK);
	}

	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	@ApiOperation(value = "Creates a new Post", notes = "The newly created post Id will be sent in the location response header", response = Void.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Post Created Successfully", response = Void.class),
			@ApiResponse(code = 500, message = "Error creating Post", response = ErrorDetail.class) })
	public ResponseEntity<?> postAction(@Valid @RequestBody Post post) {
		post = postRepository.save(post);

		// Set the location header for the newly created resource
		HttpHeaders responseHeaders = new HttpHeaders();
		URI newPostUri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(post.getId())
				.toUri();
		responseHeaders.setLocation(newPostUri);

		return new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/posts/{postId}", method = RequestMethod.GET)
	@ApiOperation(value = "Retrieves a Post associated with the postId", response = Post.class)
	public ResponseEntity<?> getAction(@PathVariable Long postId) {
		verifyPost(postId);
		Post post = postRepository.findOne(postId);
		return new ResponseEntity<>(post, HttpStatus.OK);
	}

	@RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateAction(@RequestBody Post post, @PathVariable Long postId) {
		// Save the entity
		verifyPost(postId);
		post.setId(postId);
		Post postTemp = postRepository.save(post);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteAction(@PathVariable Long postId) {
		verifyPost(postId);
		postRepository.delete(postId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private void verifyPost(Long postId) throws ResourceNotFoundException {
		Post post = postRepository.findOne(postId);
		if (post == null) {
			throw new ResourceNotFoundException("Post with id " + postId + " not found");
		}
	}

}
