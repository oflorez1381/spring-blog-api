package com.blog.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.mangofactory.swagger.plugin.EnableSwagger;

@SpringBootApplication
@EnableSwagger

public class SpringBlogApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBlogApiApplication.class, args);
	}
}
