package com.blog.api.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;

@Entity
public class Post {

	@Id
	@GeneratedValue
	private Long id;

	@NotEmpty
	private String title;

	@NotEmpty
	private String content;

	@Min(1)
	private Integer status;

	/*
	 * @Column
	 * 
	 * @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	 * private DateTime createDate;
	 * 
	 * @Column
	 * 
	 * @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	 * private DateTime updateDate;
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	/*
	 * public DateTime getCreateDate() { return createDate; }
	 * 
	 * public void setCreateDate(DateTime createDate) { this.createDate =
	 * createDate; }
	 * 
	 * public DateTime getUpdateDate() { return updateDate; }
	 * 
	 * public void setUpdateDate(DateTime updateDate) { this.updateDate =
	 * updateDate; }
	 */

}
